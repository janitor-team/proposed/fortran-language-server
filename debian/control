Source: fortran-language-server
Maintainer: Python Applications Packaging Team <python-apps-team@lists.alioth.debian.org>
Uploaders: Denis Danilov <danilovdenis@yandex.ru>
Section: editors
Priority: optional
Build-Depends: debhelper-compat (=12),
 dh-python,
 help2man,
 python3,
 python3-setuptools,
Standards-Version: 4.5.0
Homepage: https://github.com/hansec/fortran-language-server
Vcs-Git: https://salsa.debian.org/python-team/applications/fortran-language-server.git
Vcs-Browser: https://salsa.debian.org/python-team/applications/fortran-language-server

Package: fortran-language-server
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends},
Recommends: ${python:Recommends}
Suggests: ${python:Suggests}, elpa-lsp-mode
Description: Fortran Language Server for the Language Server Protocol
 Fortran Language Server (fortls) is an implementation of the Language
 Server Protocol. It can be used with editors that supports the
 protocol (e.g. Emacs with elpa-lsp-mode) to offer support for code
 completion and documentation.
 .
 Supported LSP features include:
  * Document symbols (textDocument/documentSymbol)
  * Auto-complete (textDocument/completion)
  * Signature help (textDocument/signatureHelp)
  * GoTo/Peek definition (textDocument/definition)
  * Hover (textDocument/hover)
  * GoTo implementation (textDocument/implementation)
  * Find/Peek references (textDocument/references)
  * Project-wide symbol search (workspace/symbol)
  * Symbol renaming (textDocument/rename)
  * Documentation parsing (Doxygen and FORD styles)
  * Diagnostics
